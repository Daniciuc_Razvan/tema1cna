import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import proto.ClientsGrpc;
import proto.ClientsOuterClass;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8999).usePlaintext().build();

        ClientsGrpc.ClientsStub clientsStub = ClientsGrpc.newStub(channel);

        System.out.println("...........MENU...........");
        System.out.println("1. Enter your name and ID");
        System.out.println("2. Quit");

        boolean isConnected =true;
        while(isConnected){
            Scanner input = new Scanner(System.in);
            System.out.println("Choose: ");
            int option=input.nextInt();

            switch(option){
                case 1:{
                    Scanner read= new Scanner(System.in).useDelimiter("\n");
                    System.out.println("Your name is: ");
                    String name = read.next();

                    System.out.println("Your ID is: ");
                    String ID = read.next();

                    clientsStub.sendInfo(ClientsOuterClass.ClientRequest.newBuilder().setName(name).setID(ID).build(),
                            new StreamObserver<ClientsOuterClass.ClientResponse>() {
                                @Override
                                public void onNext(ClientsOuterClass.ClientResponse clientResponse) {
                                    System.out.println(clientResponse);
                                }

                                @Override
                                public void onError(Throwable throwable) {
                                    System.out.println("Error: " + throwable.getMessage());
                                }

                                @Override
                                public void onCompleted() {

                                }
                            });
                    break;
                }
                case 2:{
                    isConnected = false;
                    break;
                }
                default:
                    System.out.println("Unknown command, insert a valid command!");
            }
        }
        channel.shutdown();
    }
}
