package Services;

import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import proto.ClientsGrpc;
import proto.ClientsOuterClass;

import java.time.LocalDate;
import java.time.Period;

public class Clients extends ClientsGrpc.ClientsImplBase {
    @Override
    public void sendInfo(ClientsOuterClass.ClientRequest request, StreamObserver<ClientsOuterClass.ClientResponse> responseObserver) {
        System.out.println("Name: " + request.getName() + "\nID: " + request.getID());
        ClientsOuterClass.ClientResponse.Builder response = ClientsOuterClass.ClientResponse.newBuilder();

        if(request.getName().equals("") || (request.getID().length() < 7 || request.getID().length() > 13)){
            Status status = Status.INVALID_ARGUMENT.withDescription("Invalid Data!");
            responseObserver.onError(status.asRuntimeException());
        }

        response.setName(request.getName());

        if(request.getID().charAt(0) % 2 == 0){
            response.setGender("Female");
        }
        else{
            response.setGender("Male");
        }

        String date;
        if(Integer.parseInt(request.getID().substring(1, 3)) > LocalDate.now().getYear() % 100)
            date = "19" + request.getID().substring(1, 3) + "-" + request.getID().substring(3, 5) +
                    "-" + request.getID().substring(5, 7);
        else date = "20" + request.getID().substring(1, 3) + "-" + request.getID().substring(3, 5) +
                    "-" + request.getID().substring(5, 7);

        Period period = Period.between(LocalDate.parse(date), LocalDate.now());
        response.setAge(period.getYears());

        responseObserver.onNext(response.build());
        System.out.println("\n" + response + "\n");
        responseObserver.onCompleted();
    }
}
